const bitmex = require('./bitmex');

const getCandles = async (count, partial) => {
  const res = await bitmex.getCandles({ count, partial });
  const closes = res.data.map((candle) => {
    return candle.close;
  });

  console.log(res.data);
  console.log(res.data.length);
  return closes;
}

function cropArray(array, length) {
  if (!length) return array;
  if (length > array.length) {
    console.log(`utils.cropArray :: Asked length (${length}) is greater than array length (${array.length})`);
    return array;
  }
  return length < array.length ? array.slice(0, length) : array;
}

const sma = (closes, length) => {
  const crop = cropArray(closes, length);
  const sum = crop.reduce((acc, close) => acc + close, 0);
  return sum / crop.length;
};


const wma = (closes, length) => {
  const dataset = cropArray(closes, length);
  const len = dataset.length;

  const values = dataset.reduce((acc, close, index) => {
    const weight = len - index;
    acc.norm += weight;
    acc.sum += close * weight;
    return acc;
  }, { norm: 0, sum: 0 });

  return values.sum / values.norm;
};

// validate hull
getCandles(18, true)
  .then((closes) => {
    const hullLength = 9;
// console.log(JSON.stringify(closes));
    const hullSeries = closes.reduce((acc, c, index) => {
      if (index > hullLength) return acc;
      const split = closes.slice(index, hullLength + index);
      // console.log(JSON.stringify(split));
      // console.log((2 * wma(split, Math.ceil(hullLength / 2))), '--', wma(split, hullLength));
      // if (split.length < len) return acc;
      acc.push((2 * wma(split, Math.floor(hullLength / 2))) - wma(split, hullLength));

      return acc;
    }, []);

    const hullSqrt = Math.round(Math.sqrt(hullLength));
    return wma(hullSeries, hullSqrt);
  })
  .then((result) => console.log(result));
