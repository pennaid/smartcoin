const _ = require('lodash');
const axios = require('axios');
const moment = require('moment');

const { model: Candle } = require('../../models/Candle');

const API_URL = 'https://api.bitfinex.com/v2/';
const PAIR = 'tBTCUSD';

const getCandles = async(num = 128, gran = '1h', divider = 4) => {
  const m = moment().subtract(128 * 3600, 'seconds');

  const query = {
    method: 'GET',
    url: `${API_URL}/candles/trade:${gran}:${PAIR}/hist`,
    params: { limit: num, sort: 1, start: m.toDate().getTime() },
  };

  try {
    const candles = (await axios(query)).data;

    const reducedCandles = candles.reduce((result, candle, index) => {
      const ind = Math.floor(index / divider);

      const newResult = result;
      if (!newResult[ind]) newResult[ind] = [];
      newResult[ind].push(candle);

      return newResult;
    }, []).map((candle) => {
      const time = candle[0][0];
      const low = _.min(candle.map((c) => c[1]));
      const max = _.max(candle.map((c) => c[2]));
      const open = candle[0][3];
      const close = candle[divider - 1][3];
      const volume = candle.reduce((sum, c) => sum + c[4], 0);

      return [time, low, max, open, close, volume];
    });

    Promise.all(reducedCandles.map(async(candle, i) => {
      const time = await Candle.findOne({ time: candle[0], exchange: 'bitfinex', granularity: 14400 });

      if (i === reducedCandles.length - 1) {
        if (time) await Candle.remove({ time: candle[0], exchange: 'gdax', granularity: 14400 });
      }

      if (!time && i !== reducedCandles.length - 1) return null;

      return Candle.create({
        time: Number(candle[0]),
        low: Number(candle[1]),
        high: Number(candle[2]),
        open: Number(candle[3]),
        close: Number(candle[4]),
        volume: Number(candle[5]),

        exchange: 'bitfinex',
        granularity: 14400, // We need to calculate this better
      });
    }));

    return reducedCandles;
  } catch (e) { return null; }
};

module.exports = { getCandles };
