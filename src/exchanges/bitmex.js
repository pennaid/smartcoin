/* eslint-disable consistent-return */
const WebSocket = require('ws');
const handlers = require('./bitmexHandlers');
const crypto = require('crypto');

const { logger } = require('../helpers');

const { BITMEX_PROD } = process.env;

const privateChannels = [
  'order',
  'position',
  'margin',
];
const publicChannels = [
  'orderBookL2:XBTUSD',
  'trade:XBTUSD',
];

const baseUrl = BITMEX_PROD === 'true' ? 'wss://www.bitmex.com/realtime?subscribe=' :
  'wss://testnet.bitmex.com/realtime?subscribe=';

const privateQuery = privateChannels.join(',');
const publicQuery = publicChannels.join(',');

const getAuthentication = ({ key, secret }) => {
  const nonce = Date.now();
  const message = `GET/realtime${nonce}`;
  return {
    'api-nonce': nonce,
    'api-key': key,
    'api-signature': crypto.createHmac('sha256', secret).update(message).digest('hex'),
  };
};

const subscribeChannels = (ws, channels) => ws.send(JSON.stringify({
  op: 'subscribe',
  args: [channels.join(',')],
}));

const heartbeat = ({ ws, key, secret, accountId, createEvent, cnx }) => setInterval(() => {
  if (!ws.isAlive) {
    clearInterval(ws.pingInterval);
    return ws.close(cnx({
      key,
      secret,
      createEvent,
      accountId,
    }));
  }
  ws.isAlive = false; // eslint-disable-line no-param-reassign
  ws.ping();
}, 5000);

const privateCnx = ({ key, secret, accountId, createEvent }) => {
  let deadMan;
  let connectedChannels = 0;

  const ws = new WebSocket(`${baseUrl}${privateQuery}`, { headers: getAuthentication({ key, secret }) });

  const autoCancel = () => setInterval(() => ws.send('{ "op": "cancelAllAfter", "args": 30000 }'), 10000);

  ws.on('error', (error) => {
    if (error.code === 'EAI_AGAIN') return setTimeout(() => privateCnx({ key, secret, accountId, createEvent }), 5000);
    logger.error('Bitmex WS Unhandled Error: ', error);
  });

  ws.on('open', () => {
    logger.info('Bitmex private connected');
    ws.pingInterval = heartbeat({ ws, key, secret, accountId, createEvent, cnx: privateCnx });
    // If all channel are not subscribed in 10s, reset WS
    ws.allSubscribedTimeout = setTimeout(() => {
      console.log('missing subscription to private channels...');
      ws.close(privateCnx({ key, secret, accountId, createEvent }));
    }, 10000);
    deadMan = autoCancel();
    ws.isAlive = true;
  });

  ws.on('pong', () => {
    ws.isAlive = true;
  });

  ws.on('close', (data) => {
    createEvent({ type: 'BITMEX_PRIVATE_CONNECTION', accountId, connected: false });
    clearInterval(deadMan);
    logger.info(`Bitmex private connection \x1b[33mclosed\x1b[0m (accountId: ${accountId})`, data);
  });

  ws.on('message', (data) => {
    clearTimeout(ws.pingInterval);
    ws.pingInterval = heartbeat({ ws, key, secret, accountId, createEvent, cnx: privateCnx });
    const json = JSON.parse(data);
    if (json.info || json.success) return;
    if (json.action === 'partial') {
      connectedChannels += 1;
      if (connectedChannels === privateChannels.length) {
        console.log('got all public channels');
        clearTimeout(ws.allSubscribedTimeout);
        createEvent({ type: 'BITMEX_PRIVATE_CONNECTION', accountId, connected: true });
      }
    }

    if (json.error) {
      logger.warn(`Subscription error: ${json.error}`);
      return subscribeChannels();
    }

    if (json.table === 'order') return handlers.handleOrder(json, createEvent);
    if (json.table === 'position') return handlers.handlePosition(json, createEvent);
    if (json.table === 'margin') return handlers.handleMargin(json, createEvent);
    if (json.cancelTime) return;

    logger.warn(`Bitmex :: Unhandled table -> ${json.table}`, data);
  });
};

const publicCnx = ({ createEvent }) => {
  let connectedChannels = 0;
  const ws = new WebSocket(`${baseUrl}${publicQuery}`);

  ws.on('open', () => {
    logger.info('Bitmex connected');
    ws.isAlive = true;
    ws.allSubscribedTimeout = setTimeout(() => {
      console.log('missing subscription to public channels...');
      ws.close(publicCnx({ createEvent }));
    }, 10000);
    ws.pingInterval = heartbeat({ ws, createEvent, cnx: publicCnx });
  });

  ws.on('pong', () => {
    ws.isAlive = true;
  });

  ws.on('close', (data) => {
    createEvent({ type: 'BITMEX_PUBLIC_CONNECTION', connected: false });
    logger.info('Bitmex public connection \x1b[33mclosed\x1b[0m', data);
  });

  ws.on('message', (data) => {
    clearInterval(ws.pingInterval);
    ws.pingInterval = heartbeat({ ws, createEvent, cnx: publicCnx });
    const json = JSON.parse(data);
    if (json.info) return;
    if (json.success) {
      connectedChannels += 1;
      if (connectedChannels === publicChannels.length) {
        clearTimeout(ws.allSubscribedTimeout);
        createEvent({ type: 'BITMEX_PUBLIC_CONNECTION', connected: true });
      }
      return;
    }

    if (json.error) {
      logger.warn(`Subscription error: ${json.error}`);
      return subscribeChannels();
    }

    if (json.table === 'trade') return handlers.handleTrade(json, createEvent);
    if (json.table === 'orderBookL2') return handlers.handleOrderbook(json, createEvent);

    logger.warn(`Bitmex :: Unhandled table -> ${json.table}`, data);
  });
};

module.exports = {
  privateCnx,
  publicCnx,
};
