/* eslint-disable consistent-return */
const { logger } = require('../helpers');

const handleOrderbook = (info, createEvent) => {
  switch (info.action) {
    case 'partial': {
      const received = info.data;
      if (received.length) return createEvent({ type: 'RECEIVED_INITIAL_ORDERBOOK', received });
      break;
    }
    case 'update': {
      return createEvent({ type: 'UPDATE_ORDERBOOK', received: info.data });
    }
    case 'insert': {
      return createEvent({ type: 'INSERT_ORDERBOOK', received: info.data });
    }
    case 'delete': {
      return createEvent({ type: 'DELETE_ORDERBOOK', received: info.data });
    }
    default:
      logger.warn(`Bitmex:: Missing case for Orderbooks: ${info.action}`);
      break;
  }
};

const handleTrade = (info, createEvent) => {
  switch (info.action) {
    case 'partial':
    case 'insert': return;
    default:
      logger.warn(`Bitmex:: Missing case for Trade: ${info.action}`);
      break;
  }
};

const handlePosition = (info, createEvent) => {
  switch (info.action) {
    case 'partial': {
      const received = info.data.find((d) => d.isOpen) || {};
      if (!info.data[0]) return;
      return createEvent({ type: 'RECEIVED_INITIAL_POSITION', received, id: info.data[0].account });
    }
    case 'update': {
      const received = info.data[0];
      return createEvent({ type: 'UPDATE_POSITION', received, id: received.account });
    }
    default:
      logger.warn(`Bitmex :: Missing case for Position: ${info.action}`);
      break;
  }
};

const handleOrder = (info, createEvent) => {
  switch (info.action) {
    case 'partial': {
      const received = info.data;
      if (received.length) return createEvent({ type: 'RECEIVED_INITIAL_ORDERS', received, id: received[0].account });
      break;
    }
    case 'update': {
      const received = info.data;
      return createEvent({ type: 'UPDATE_ORDER', received, id: received[0].account });
    }
    case 'insert': {
      const received = info.data;
      return createEvent({ type: 'INSERT_ORDER', received: info.data, id: received[0].account });
    }
    default:
      logger.warn(`Bitmex:: Missing case for Order: ${info.action}`);
      break;
  }
};

const handleMargin = (info, createEvent) => {
  switch (info.action) {
    case 'partial': {
      const received = info.data[0];
      if (received) return createEvent({ type: 'RECEIVED_INITIAL_MARGIN', received, id: received.account });
      break;
    }
    case 'update': {
      const received = info.data[0];
      return createEvent({ type: 'UPDATE_MARGIN', received, id: received.account });
    }
    default:
      logger.warn(`Bitmex:: Missing case for Order: ${info.action}`);
      break;
  }
};

module.exports = {
  handleOrderbook,
  handleTrade,
  handleMargin,
  handlePosition,
  handleOrder,
};
