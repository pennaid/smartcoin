const Telegraf = require('telegraf');

const Logger = require('../helpers/logger');

module.exports = () => {
  const notificationBot = new Telegraf(process.env.NOTIFICATION_BOT_TOKEN);

  notificationBot.telegram.setWebhook(process.env.WEBHOOK_URL || 'https://kelvne.ngrok.io');
  notificationBot.startWebhook('/', null, process.env.TELEGRAM_PORT || 8085);

  Logger.info('app.boot.notificationBot :: Telegram bot set succesfully.');

  return { bot: notificationBot };
};
