const { redis, main } = require('../config');

const { client } = redis;
const { TURNED_ON_TOKEN } = main;
const { telegram, logger, mq } = require('../helpers');

const { events: brokerEvent } = require('../broker');

module.exports = (queue, notificationBot, runtime) => {
  const { createEvent } = mq(queue);

  queue.process(async(job, done) => {
    const broker = await brokerEvent(createEvent, job, runtime, notificationBot);
    if (broker) return done(null, true);

    const { type } = job.data;
    switch (type) {
      case 'SEND_MESSAGE_TO_GROUP': {
        const { message, logType } = job.data;
        telegram(notificationBot).sendMessage(message || 'empty message', logType);
        break;
      }

      case 'TURN_ON_BOT': {
        if (job.data.token === TURNED_ON_TOKEN) {
          client.set('BOT_STATE', TURNED_ON_TOKEN, (err) => {
            if (!err) {
              logger.info('BOT IS ON.');
              telegram(notificationBot).sendMessage(`${new Date()} :: BOT IS ON`);
            }
          });
        }
        break;
      }

      case 'TURN_OFF_BOT': {
        client.set('BOT_STATE', 'OFF', (err) => {
          if (!err) telegram(notificationBot).sendMessage(`${new Date()} :: BOT IS OFF`);
        });
        break;
      }

      default:
        break;
    }

    return done(null, true);
  });

  logger.info('app.boot.events :: Queue events prepared successfully.');
};
