/* eslint-disable consistent-return */
const Promise = require('bluebird');
const { logger } = require('../helpers');

const bitmex = require('../exchanges/rest/bitmex');

const getXBTPositions = async({ clients, pair = 'XBTUSD' }) => {
  try {
    const userPositions = clients.map((client) => {
      const { key, secret } = client;
      return bitmex.getPositions({
        key,
        secret,
        pair,
      });
    });

    return await Promise.reduce(userPositions, (acc, position) => {
      if (!position.length) return acc;
      const pos = position[0];
      acc[pos.account] = {
        accountId: pos.account,
        pair: pos.symbol,
        side: pos.currentQty < 0 ? 'short' : 'long',
        commission: pos.commission,
        initialMargin: pos.initMarginReq,
        maintenanceMargin: pos.maintMarginReq,
        riskLimit: pos.riskLimit,
        leverage: pos.leverage,
        contracts: pos.currentQty,
        xbt: pos.simpleQty,
        markPrice: pos.markPrice,
        markValue: pos.markValue,
      };
      return acc;
    }, {});
  } catch (e) {
    logger.error(`fetcher.getXBTPositions :: Error: ${e}`);
  }
};

const getXBTOpenOrders = async({ clients, pair = 'XBTUSD' }) => {
  try {
    const userOpenOrders = await clients.map((client) => {
      const { key, secret } = client;
      return bitmex.getOpenOrders({
        key,
        secret,
        pair,
      });
    });

    return Promise.reduce(userOpenOrders, (acc, orders) => {
      if (!orders[0].account) return acc;
      acc[orders[0].account] = orders.map((order) => ({
        accountId: order.account,
        orderId: order.orderID,
        clientOrderId: order.clOrdID,
        side: order.side,
        pair: order.symbol,
        orderQty: order.orderQty,
        stopPrice: order.stopPx,
        price: order.price,
        orderType: order.ordType,
        timeInForce: order.timeInForce,
        instructions: order.execInst,
        status: order.orderStatus,
        triggered: order.triggered,
        simpleAssetsQty: order.simpleLeavesQty,
        timestamp: order.timestamp,
      }));
      return acc;
    }, {});
  } catch (e) {
    logger.error(`fetcher.getXBTOpenOrders :: Error: ${e}`);
  }
};

const getUsersWallets = async({ clients }) => {
  try {
    const usersWallets = await clients.map((client) => {
      const { key, secret } = client;
      return bitmex.getMargin({
        key,
        secret,
      });
    });

    return Promise.reduce(usersWallets, (acc, w) => {
      acc[w.account] = {
        accountId: w.account,
        riskLimit: w.riskLimit,
        walletBalance: w.wBalance,
        marginBalance: w.marginBalance,
        marginUsedPcnt: w.marginUsedPcnt,
        marginExcessPcnt: w.excessMarginPcnt,
        marginAvailable: w.availableMargin,
        marginLeverage: (w.marginLeverage * w.excessMarginPcnt) / w.marginUsedPcnt,
      };
      return acc;
    }, {});
  } catch (e) {
    logger.error(`fetcher.getUsersWallets :: Error: ${e}`);
  }
};

const getAvailableXBT = async({ clients }) => {
  try {
    return await Promise.all(clients.map((client) => {
      const { key, secret } = client;
      return bitmex.getMargin({
        key,
        secret,
      });
    }));
  } catch (e) {
    return logger.error('fetcher.getAvailableXBT :: Could not get wallets', e);
  }
};

const getClientData = async({ key, secret }) => {
  try {
    const user = await bitmex.getUser({ key, secret });
    return {
      accountId: user.id,
      firstname: user.firstname,
      lastname: user.lastname,
      email: user.email,
      phone: user.phone,
      country: user.country,
    };
  } catch (e) {
    return logger.error('fetcher.getClientData :: Could not get client data', e);
  }
};

const getFundingRate = async() => {
  try {
    const instrument = await bitmex.getInstrument();
    return instrument.fundingRate;
  } catch (e) {
    return logger.error('fetcher.getFundingRate :: Could not get funding data', e);
  }
};

module.exports = {
  getXBTPositions,
  getXBTOpenOrders,
  getUsersWallets,
  getClientData,
  getAvailableXBT,
  getFundingRate,
};
