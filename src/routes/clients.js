const { Router } = require('express');
const { model: Client } = require('../models/Client');

const brokerFetcher = require('../broker/fetcher');

const { BITMEX_PROD } = process.env;

const router = Router();

module.exports = (mqHelpers) => {
  router.get('/', async(req, res) => {
    try {
      const clients = await Client.getAllClients();
      res.status(200).send({ success: true, clients });
    } catch (e) {
      res.status(500).send({ success: false, error: e.message });
    }
  });

  router.post('/', async(req, res) => { // eslint-disable-line consistent-return
    const { label, key, secret, leverage, budget } = req.body;
    try {
      const user = await brokerFetcher.getClientData({ key, secret });
      const client = await Client.create({
        key,
        secret,
        label,
        leverage,
        budget,
        test: BITMEX_PROD !== 'true',
        ...user,
        accountId: user.accountId,
      });
      res.status(201).send({
        success: true,
        client: {
          _id: client._id,
          label: client.label,
          accountId: client.accountId,
          firstname: client.firstname,
          lastname: client.lastname,
          leverage: client.leverage,
          budget: client.budget,
          test: client.test,
        },
      });
    } catch (e) {
      console.log(e);
      if (e.name === 'VaidationError') return res.status(400).send({ success: false, error: e.message });
      res.status(500).send({ success: false, error: e.message });
    }
  });

  router.put('/leverage', async(req, res) => {
    const { _id, leverage } = req.body;
    if (!_id || !leverage) return res.status(400).send({ success: false, error: 'Param _id and leverage is required.' });
    try {
      const client = await Client.findOneAndUpdate(
        { _id },
        { leverage },
        {
          new: true,
          runValidators: true,
          select: '_id label accountId firstname lastname leverage budget',
        },
      );
      if (!client) return res.status(201).send({ success: false, error: `No client with this ID: ${_id}` });
      return res.status(201).send({ success: true, client });
    } catch (e) {
      return res.status(500).send({ success: false, error: e.message });
    }
  });

  router.put('/budget', async(req, res) => {
    const { _id, budget } = req.body;
    if (!_id || !budget) return res.status(400).send({ success: false, error: 'Param _id and budget is required.' });
    try {
      const client = await Client.findOneAndUpdate(
        { _id },
        { budget },
        {
          new: true,
          runValidators: true,
          select: '_id label accountId firstname lastname leverage budget',
        },
      );
      if (!client) return res.status(201).send({ success: false, error: `No client with this ID: ${_id}` });
      mqHelpers.createEvent({ type: 'UPDATE_CLIENT', accountId: client.accountId });
      return res.status(201).send({ success: true, client });
    } catch (e) {
      return res.status(500).send({ success: false, error: e.message });
    }
  });

  router.delete('/', async(req, res) => { // eslint-disable-line consistent-return
    const { _id } = req.body;
    if (!_id) return res.status(400).send({ success: false, error: 'Param _id is required.' });
    try {
      const c = await Client.remove({ _id });
      if (c.result.n > 0) return res.status(201).send({ success: true });
      res.status(201).send({ success: false, error: `No client with this ID: ${_id}` });
    } catch (e) {
      res.status(500).send({ success: false, error: e.message });
    }
  });

  return router;
}
