const { Router } = require('express');
const { model: User } = require('../models/User');

const router = Router();

router.post('/', async(req, res) => {
  const { headers } = req;

  const userAuth = process.env.USER_AUTH_TOKEN;

  if (!headers['x-user-auth'] || headers['x-user-auth'] !== userAuth) {
    return res.sendStatus(401);
  }

  const { email, password } = req.body;

  if (!email && !password) return res.sendStatus(400);

  try {
    await User.create({ email, password });
  } catch (e) {
    return res.sendStatus(500);
  }

  return res.sendStatus(200);
});

router.post('/auth', async(req, res) => {
  const { email, password } = req.body;

  if (!email && !password) return res.sendStatus(400);

  const user = await User.findOne({ email });

  if (!user) return res.sendStatus(404);

  return user.comparePassword(password, (err, match) => {
    if (err) return res.sendStatus(401);
    if (!match) return res.sendStatus(401);
    return res.sendStatus(200);
  });
});

module.exports = router;
