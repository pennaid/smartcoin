const pristineCreateEvent = (queue) => async(data) => {
  try {
    const event = await queue.createJob(data).save();
    return event;
  } catch (e) { return null; }
};

module.exports = (queue) => {
  const createEvent = pristineCreateEvent(queue);

  return { createEvent };
};
